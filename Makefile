.PHONY: all paper clean

all: paper

paper:
	@$(MAKE) -C ./paper pdf

clean:
	@$(MAKE) -C ./paper clean