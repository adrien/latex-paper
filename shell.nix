{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = [ 
      (pkgs.texlive.combine { 
        inherit(pkgs.texlive)
        scheme-medium
        biber
        biblatex;
      })
      pkgs.gnumake
     ];
}
